// Soal 1
var nama = "Serius"
var peran = ""

if (nama == ""){
    console.log("Nama harus diisi!");
} else {
    if (peran == ""){
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
    } else{
        if (peran == "Penyihir"){
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
        } else if (peran == "Guard"){
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        } else if (peran == "Werewolf"){
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
        }
    }
}

// Soal 2
var hari = 29; 
var bulan = 2; 
var tahun = 2020;

switch(bulan){
    case 1 : {
        bulan = "Januari";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 2 : {
        bulan = "Februari";
        if ((tahun%4) == 0){
            if ((hari>=1 && hari<=29) && (tahun>=1900 && tahun<=2020)){
                console.log(hari + " " + bulan + " " + tahun);
            }
        }else{
            if ((hari>=1 && hari<=28) && (tahun>=1900 && tahun<=2020)){
                console.log(hari + " " + bulan + " " + tahun);
            }
        }   
        break;
    }
    case 3 : {
        bulan = "Maret";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 4 : {
        bulan = "April";
        if ((hari>=1 && hari<=30) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 5 : {
        bulan = "Mei";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 6 : {
        bulan = "Juni";
        if ((hari>=1 && hari<=30) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 7 : {
        bulan = "Juli";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 8 : {
        bulan = "Agustus";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 9 : {
        bulan = "September";
        if ((hari>=1 && hari<=30) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 10 : {
        bulan = "Oktober";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 11 : {
        bulan = "November";
        if ((hari>=1 && hari<=30) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    case 12 : {
        bulan = "Desember";
        if ((hari>=1 && hari<=31) && (tahun>=1900 && tahun<=2020)){
            console.log(hari + " " + bulan + " " + tahun);
        }
        break;
    }
    default : {
        break;
    }
}
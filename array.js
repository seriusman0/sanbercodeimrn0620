// Soal 1
function range(startNum, finishNum) {
    var container = [];
    if (startNum<finishNum){
        var panjang = finishNum-startNum;
        for (var i=0; i<=panjang; i++){
            container.push(startNum + i);
        }
    } else if(startNum>finishNum){
        var panjang = startNum-finishNum;
        for (var i=0; i<=panjang; i++){
            container.push(startNum - i);
        }
    } else {
        container = -1;
    }
    
    return container;
}


console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2
function rangeWithStep(startNum, finishNum, step) {
    var container = [];
    if(startNum<finishNum){
        panjang = parseInt((finishNum-startNum)/step+1); 
        for(var i=0; i<panjang; i++){
            container.push(startNum+step*i);
        }
    }else{
        panjang = parseInt((startNum-finishNum)/step+1);
        for(var i=0; i<panjang; i++){
            container.push(startNum-step*i);
        }
    }
    return container;
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
function rangeWithStep(startNum, finishNum, step) {
    var container = [];
    if(startNum<finishNum){
        panjang = parseInt((finishNum-startNum)/step+1); 
        for(var i=0; i<panjang; i++){
            container.push(startNum+step*i);
        }
    }else if (startNum>finishNum){
        panjang = parseInt((startNum-finishNum)/step+1);
        for(var i=0; i<panjang; i++){
            container.push(startNum-step*i);
        }
    }
    
    return container;
}

function sum(firstNum, lastNum, distance){
    if (firstNum == 1 && lastNum == undefined && distance == undefined){
        return 1;
    }else if(firstNum == undefined && lastNum == undefined && distance == undefined){
        return 0;
    }else if(distance == undefined){
        var matriks = rangeWithStep(firstNum, lastNum, 1);
    }
    else{
        var matriks = rangeWithStep(firstNum, lastNum, distance);
    }
    
    var panjang = matriks.length;
    var total = 0;
    for (var i=0; i<panjang; i++)(
        total+=matriks[i]
    )
    return total;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(input){
    for (var i=0; i<input.length; i++){
        console.log("Nomor ID: "+ input[i][0]);
        console.log("Nama Lengkap:  "+ input[i][1]);
        console.log("TTL:  "+ input[i][2] + " " + input[i][3]);
        console.log("Hobi: "+ input[i][4]);
        console.log();

    }   
}

dataHandling(input);    

// Soal 5
function balikKata(kata){
    var panjang = kata.length;
    var katabalik =[];
    for (var i=panjang-1; i>=0; i--){
        katabalik.push(kata[i]);
    }
    return katabalik;
}


console.log(balikKata("Kasur Rusak"))// kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(data);

function identitas(data){
    data.splice(1,1,(data[1] + " " + "Elsharawy"));
    data.splice(2, 1, ("Provinsi " + data[2]));
    data.splice(4, 0, "Pria");
    data.splice(5, 1, "SMA Internasional Metro");
    return console.log(data);
}

function formattanggal(mtanggal){
    var sementara;
    sementara = mtanggal[0];
    mtanggal[0] = mtanggal[2];
    mtanggal[2] = sementara;
    sementara = mtanggal[1];
    mtanggal[1] = mtanggal[2];
    mtanggal[2] = sementara;
    return mtanggal
}

function namabulan(tanggal){
    var bulan = Number(tanggal[1]);
    switch (bulan){
        case 1: {
            bulan = "Januari";
            break;
        }
        case 2: {
            bulan = "Februari";
            break;
        }
        case 3: {
            bulan = "Maret";
            break;
        }
        case 4: {
            bulan = "April";
            break;
        }
        case 5: {
            bulan = "Mei";
            break;
        }
        case 6: {
            bulan = "Juni";
            break;
        }
        case 7: {
            bulan = "Juli";
            break;
        }
        case 8: {
            bulan = "Agustus";
            break;
        }
        case 9: {
            bulan = "September";
            break;
        }
        case 10: {
            bulan = "Oktober";
            break;
        }
        case 11: {
            bulan = "November";
            break;
        }
        case 12: {
            bulan = "Desember";
            break;
        }
    }
    return bulan;
}

function dataHandling2(input){
    
    identitas(data);
    var tanggal = data[3].split("/");
    console.log(namabulan(tanggal));
    console.log(formattanggal(tanggal));
    console.log(tanggal.join("-"));
    console.log(String(data[1].slice(0, 14)));
}

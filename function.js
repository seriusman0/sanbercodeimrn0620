// Soal 1
function teriak(){
    return "Halo Sanbers!";
}
 
console.log(teriak())

// Soal 2
function kalikan(a, b){
    return a*b;
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// Soal 3
function introduce(nama, umur, alamat, hobby){
    return "Nama saya "+ nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobby + "!" ;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
// Soal 1
// Release 0
class Animal {
    constructor (name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Soal 2
// Release 1
class Ape extends Animal {
    constructor(name) {
      super(name);
      this.yell2 = "Auooo";
      this.legs = 2;
    }
    yell() {
        return this.yell2;
    }
}

var sungokong = new Ape;("kera sakti")
console.log(sungokong.yell()); // "Auooo"

class Frog extends Animal {
    constructor(name) {
      super(name);
      this.jump1 = "hop hop";
    }
    jump() {
        return this.jump1;
    }
}

var kodok = new Frog;("kera sakti")
console.log(kodok.jump()); // "hop hop"

// Soal 2
class Clock {
    contructor (template){

    }
    class timer = {
  
            render() {
            var date = new Date();
        
            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
        
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
        
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;
        
            var output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);
        
            console.log(output);
            }
        
            this.stop = berhenti() {
            clearInterval(timer);
            };
        
            this.start = mulai() {
            render();
            timer = setInterval(render, 1000);
            };
    }
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
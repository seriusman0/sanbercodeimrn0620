// Soal 1
function arrayToObject(data) {
    if (!data[1][3]){
        data[1][3] = 0;
    }
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    var  data1 = {
        firstName : data[0][0],
        lastName : data[0][1],
        gender : data[0][2],
        age : thisYear - data[0][3]
    }

    var  data2 = {
        firstName : data[1][0],
        lastName : data[1][1],
        gender : data[1][2],
        age : thisYear - data[1][3]
    }

    var namaLengkapData1 = data[0][0] + " " + data[0][1]; 
    var namaLengkapData2 = data[1][0] + " " + data[1][1]; 

    console.log("1. " + namaLengkapData1 + " : " );
    console.log(data1);
    console.log("2. " + namaLengkapData2 + " : " );
    console.log(data2);
}

var data = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male"]];
arrayToObject(data);


// Soal 2
function shoppingTime(IdMember, Uang) {
    var informasi = {
        memberId : IdMember,
        money : Uang,
        listPurchased : troli(Uang),
        changeMoney : dompet(Uang)
    }
    return informasi;

  }

  function troli(money){
      keranjang =[];
        if (money >= produk1.harga){
            keranjang.push(produk1.nama);
            money = money-produk1.harga;
        }if (money >= produk2.harga){
            keranjang.push(produk2.nama);         
            money = money-produk2.harga;
        }if (money >= produk3.harga){
            keranjang.push(produk3.nama); 
            money = money-produk3.harga;
        }if (money >= produk4.harga){
            keranjang.push(produk4.nama);
            money = money-produk4.harga;
        }if (money >= produk5.harga){
            keranjang.push(produk5.nama);
            money = money-produk5.harga;
        }
    console.log("Sisa Uang = "+money);
      return keranjang;
  }

  function dompet(money){
      if (money >= produk1.harga){
          money = money-produk1.harga;
      }if (money >= produk2.harga){
          money = money-produk2.harga;
      }if (money >= produk3.harga){
          money = money-produk3.harga;
      }if (money >= produk4.harga){
          money = money-produk4.harga;
      }if (money >= produk5.harga){
          money = money-produk5.harga;
    }
    return money;
}


var produk1 = {
    nama : "Sepatu Stacattu",
    harga : 1500000    
}

var produk2 = {
    nama : "Baju Zoro",
    harga : 500000    
}

var produk3 = {
    nama : "Baju H&N",
    harga : 250000    
}

var produk4 = {
    nama : "Sweater Uniklooh",
    harga : 175000    
}

var produk5 = {
    nama : "Casing Handphone",
    harga : 50000    
}

  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }

    console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

// Soal 3
function naikAngkot(arrPenumpang) {
    ongkos1 = hitungOngkos(arrPenumpang[0][1],arrPenumpang[0][2]);
    ongkos2 = hitungOngkos(arrPenumpang[1][1],arrPenumpang[1][2]);
    var informasi1 = {
        penumpang: arrPenumpang[0][0],
        naikDari: arrPenumpang[0][1],
        tujuan: arrPenumpang[0][2],
        bayar: ongkos1
    }

    var informasi2 = {
        penumpang: arrPenumpang[1][0],
        naikDari: arrPenumpang[1][1],
        tujuan: arrPenumpang[1][2],
        bayar: ongkos2
    }
    return (informasi2, informasi1);
  }

function hitungOngkos(asal, tujuan){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var start;
    var finish;
    var ongkos = 0;
    for (var i=0; i<6; i++){
        if (rute[i] == asal){
            start = i;
        }
        if (rute[i] == tujuan){
            finish = i;
        }
    }

    // console.log(start);
    // console.log(finish);
    

    for (var i=start; i<finish; i++){
        ongkos+=2000;
    }

    // console.log(ongkos);
    return ongkos;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

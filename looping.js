// Soal 1
var i = 0;
console.log("LOOPING PERTAMA");
while (i<20){
    i+=2;
    console.log(i + " - I love coding");
}

console.log("LOOPING KEDUA");
while (i>=2){
    console.log(i + " - I will become a mobile developer");
    i-=2;
}

//Soal 2
for(var i=1; i<=20; i++){
    if(i%3==0 && i%2!=0){
        console.log(i+ " - I Love Coding");
    }else if(i%2!=0){
        console.log(i+ " - Santai");
    }else{
        console.log(i+ " - Berkualitas");
    }
} 

//Soal 3
for(var y=0; y<4; y++){
    for(var x=0; x<8; x++){
        process.stdout.write("*");
    }console.log();
}

//Soal 4
for(var y=0; y<7; y++){
    for (var x=0; x<=y; x++){
        process.stdout.write("#");
    }console.log();
}

//Soal 5
for(var i=0; i<8; i++){
    for(var j=0; j<8; j++){
        if(i%2==0){
            if(j%2!=0){
                process.stdout.write("#");
            }else{
                process.stdout.write(" ");
            }
        }else{
            if(j%2!=0){
                process.stdout.write(" ");
            }else{
                process.stdout.write("#");
            }
        }
    }console.log();
}
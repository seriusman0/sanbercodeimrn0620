import React from 'react';
import Main from './app/components/Main';
import { View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <Main />
    )
  }
}

